# Changelog

All notable changes to this project will be documented in this file. Dates are displayed in UTC.

## [Minor] Version-0.1.1, 23 May 2024


> **Commits:**

- [[`bdcc922`]()] - feat(lang): german translation
- [[`08656aa`]()] - feat(lang): russian translation
- [[`11db15b`]()] - feat(lang): japanese translation
- [[`9c5711c`]()] - feat(lang): indonesian translation
- [[`5ca150d`]()] - feat(lang): french translation
- [[`d8b17d2`]()] - fix: language-switcher


- [] - Finishing the translation
## [Minor] Version-0.1.0, 23 May 2024


> **Commits:**

- [[`1059b77`]()] - init: migrate


